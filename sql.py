# This is where we store the SQL functions and connect to the DB

def isInDB(itemID):
    if type(itemID) != '':
        raise ValueError('isInDB - Expected argument to be a string')

    query = 'SELECT * FROM product_template WHERE x_ebay_id = $(ebay_id)s'
    c.execute(query, {'ebay_id':itemID})
    res = c.fetchone()
    con.commit()

    if type(res) != type(None):
        if itemID in res:
            return True
        else
            return False 

def sql_update_quantity(data):
    if (isinstance(data, dict) != True):
        raise ValueError('ImportToDB: Expected argument to be a dict')

    # Check if the item exists by checking its ebay_id 
    query = """
        SELECT * FROM product_template WHERE x_ebay_id = %(ebay_id)s
    """
    c.execute(query, data)
    res = c.fetchone()
    con.commit()

    if type(res) != type(None):
        if data['ebay_id'] in res: # This item already exists, therefore we should update the quantity
            ddata = data
            # Delete the stock first
            query = """
                UPDATE stock_quant SET qty = %(quantity)s WHERE product_id = %(id)s;
               UPDATE product_template SET x_qty = %(quantity)s WHERE x_ebay_id = %(ebay_id)s;
            """
            ddata['id'] = res[0]
            c.execute(query, ddata)
            ddata = None
            con.commit()

            return data
    res = None

def sql_import_template(data):
    if (isinstance(data, dict) != True):
        raise ValueError('ImportToDB: Expected argument to be a dict')    

    # Don't import anything with a quantity less than one
    if int(data['quantity']) < 1:
        print "Not importing {} because its quantity is {}".format(data['ebay_id'], data['quantity'])
        return data

    # Begin importing the data
    query = """
        INSERT INTO product_template
        (create_uid, write_uid, create_date, write_date, 
            warranty, list_price, weight, sequence, 
            uom_id, sale_ok, categ_id, company_id, 
            uom_po_id, volume, active, rental, 
            name, description_sale, type, sale_delay, 
            tracking, track_service, invoice_policy, x_manufacturer, x_part_number, x_condition, x_ebay_id, x_location)  
        VALUES(1, 1, %(create_date)s, %(create_date)s, 
            0, %(list_price)s, 0, 1, 
            1, True, 2, 1, 
            1, 0, True,  False, 
            %(name)s, %(description)s, 'product', 7, 
            'none', 'manual', 'order', %(manufacturer)s, %(part_number)s, %(condition)s, %(ebay_id)s, %(location)s)    
    """

    c.execute(query, data)
    con.commit()

    # Grab the ID produced from running the above query
    query = """
        SELECT * FROM product_template WHERE x_ebay_id = %(ebay_id)s
    """
    c.execute(query, data)
    res = c.fetchone()
    con.commit()

    data['product_tmpl_id'] = res[0]
    res = None
    # Next, we want to insert into product_product where product_tmpl_id = the id from the above query

    query = """
        INSERT INTO product_product
        (create_uid, write_uid, create_date, write_date, name_template, product_tmpl_id, active)
        VALUES(1, 1, %(create_date)s, %(create_date)s, %(name)s, %(product_tmpl_id)s, True)
    """
    c.execute(query, data)
    con.commit()

    # After the above runs, we need its id for the next two queries

    query = """
        SELECT * FROM product_product WHERE product_tmpl_id = %(product_tmpl_id)s
    """
    # Run the above query and store the result inside the cursor
    c.execute(query, data)

    # Fetch one row of data from the cursor
    res = c.fetchone()
    # Store the prodcut_product id (This ID is is always the first piece of data returned in the tuple)
    data['product_id'] = res[0]
    con.commit()

    query = """
        INSERT INTO stock_inventory
        (create_uid, write_uid, create_date, write_date, date, name, location_id, company_id, state, filter, product_id)
        VALUES(1, 1, %(create_date)s, %(create_date)s, %(create_date)s, %(name)s, %(location_id)s, 1, 'done', 'product', %(product_id)s)
    """
    c.execute(query, data)
    con.commit()

    query = """
        INSERT INTO stock_quant
        (create_uid, write_uid, create_date, write_date, in_date, product_id, cost, location_id, company_id, qty)
        VALUES(1, 1, %(create_date)s, %(create_date)s, %(create_date)s, %(product_id)s, 0, %(location_id)s, 1, %(quantity)s)
    """
    c.execute(query, data)
    con.commit()

    # Create the location for the item
    query = """
        INSERT INTO stock_location
        (create_uid, write_uid, create_date, write_date, location_id, scrap_location, company_id, usage, posz, posy, posx, active, return_location, name, complete_name, )
    """

    return data


# Attempt to connect to the database

try:
    psql = config['postgres'] # attempts to pull the postgres config from the config file
    try:
        con = db.connect('host='+psql['host']+' dbname='+psql['dbname']+' user='+psql['user']+' password='+pqsl['password']) # try to connect to the database
        c = con.cursor(name=None, cursor_factory=None, scrollable=None, withhold=False) # setup a cursor to the database to begin running queries
    except db.OperationalError:
        raise KeyError 'unable to connect to DB, check your postgres settings in config file!'
except KeyError: # it failed
    raise KeyError 'postgres settings missing from config file!'
