from email.MIMEMultipart import MIMEMultipart as multipart_email
from email.MIMEText import MIMEText as email_text
from email.MIMEBase import MIMEBase as email_base
from email import encoders

import datetime
import smtplib

fdate = datetime.datetime.strftime(datetime.datetime.today(), '%Y-%m-%d_%I_')

config = {
	'svr':{
		'host': '10.2.2.52',
		'port': 25
	},
	'from': 'odoo@outbound.erp',
	'to': [
		'support@oceantechonline.com',
	],
	'subject': 'Ebay Import Results',
	'attachment': [
		'/var/lib/odoo/import-data/log/'+fdate+'import.log'
	],
	'body': """
		<html>
			<head>
			</head>
			<body>
				<h2>Here are the results of importing from ebay</h2>
			</body>
		</html>
	"""
}

email_svr = smtplib.SMTP(config['svr']['host'], config['svr']['port'])

email_msg = multipart_email()
email_msg['From'] = config['from']
email_msg['To'] = ', '.join(config['to']) # This will allow us to send the email to multiple recipients
email_msg['Subject'] = config['subject']

email_msg.attach(email_text(config['body'], 'html'))

# Add each of the attachments to the email
for i in config['attachment']:
	attachment = open(i, 'rb')

	part = email_base('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', 'attachment; filename={}'.format(i.split('/')[-1]))

	email_msg.attach(part)

email_msg = email_msg.as_string()
email_svr.sendmail(config['from'], ', '.join(config['to']), email_msg)

email_svr.quit()