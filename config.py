# Change Configuration options here
config = {
	'ebay':{
		'application':{ # This defines our application parameters - Don't edit unless the token or keys are no longer valid
			'domain': 'api.ebay.com',
			'app_id': '',
			'dev_id': '',
			'crt_id': '',
			'user_token': '',
			'rate_limit': 20000, # Total amount of requests this application should make, attempts to stop before this number most of the time - Set in accordance with https://developer.ebay.com/my/stats?env=production&index=0
		},
		'itemID': '', # import a specific itemID from ebay
		'startDate': 'yyyy-mm-dd', # This is the date to begin importing items from ebay
		'endDate': 'yyyy-mm-dd', # This is the date to stop importing items from ebay
		'days': -20, # Go forward or backward in increment of this many days - Negative numbers go back in time, depends on the date range above
		'itemTotalsToGet': [ # This is used for getItemTotals()
			'Active', # Get total number of active items on ebay
		]
	},
	'csv': { # This defines our CSV files that we write to for importing
		'csv_delimiter': ',',
		'csv_quote': '\'',
		'files': {
			'imported_items': '',
			'failed_import': '',
			'brokerbin_import': '',
			'db_import': ''
		}
	},
	'postgres': { # This is our database credentials 
		'host': 'localhost',
		'dbname': 'postgres',
		'user': 'postgres',
		'password': ''
	},
	'ignoreErrors': False,
	'noSQL': False, 
	'noCSV': False,
	'wait_time': 5 # Time in seconds to wait between each series of requests
}