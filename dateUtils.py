# Creates a dateRange list for use with glue
def setDateRange(days=None, start=None, stop=None, rangeType=None):
    # Default to searching for the current 24 hours, minus 1ms

    # Formats the date presented to it according to what we need, defaults to today
    def checkDate(theDate=None):
        if theDate != None:
            if type(theDate) == type(datetime.datetime.today()):
                return theDate
            elif type(theDate) == type(''):
                try:
                    return datetime.datetime.strptime(theDate, '%Y-%m-%d')
                except ValueError:
                    return datetime.datetime.today()
            else:
                return datetime.datetime.today()
        else:
            return datetime.datetime.today()

    # Set our days to an int, defaults to 0
    try:
        days = int(days)
    except (TypeError, ValueError):
        days = 0

    # Set our rangeType to str, defaults to 'end'
    if rangeType != None:
        rangeType = str(rangeType)
    else: 
        rangeType = 'end'

    # Set the days argument to search forward/backward more than one day
    delta = datetime.timedelta(days)

    start_time = checkDate(start)

    if stop != None:
        end_time = checkDate(stop)
    else:
        end_time = start_time + delta
    
    # Convert our dates into a format that the api can recognize (ISO 8601)
    start_time = start_time.strftime("%Y-%m-%dT00:00:00.000Z")
    # Force the future to be the absolute end of the day
    end_time = end_time.strftime("%Y-%m-%dT23:59:59.999Z")

    # if the end_time is in the past, reverse order (EG, the delta is a negative number)
    if end_time < start_time:
        start_time_old = start_time
        start_time = end_time
        end_time = start_time_old

    return {'from': start_time, 'to': end_time, 'type': rangeType}

# We call this a couple times, so it gets its own function
def switchDateRange(list=None, range=None):
    # Switch statement to check which type of dateRange to search by
    # Will always return a valid dateRange for use as various itemArgs 
    # Defaults to a dateRange ending today

    if (range == None) | (isinstance(range, dict) != True):
        # Sets the default range (items that are ending today)
        range = setDateRange()

    if (list == None) | (isinstance(list, dict) != True):
        list = {}

    # This can be start/mod/end
    def rangeType(condition=None):
        # No condition to check, defaults to End
        if condition == None:
            return 'End'
        # Cast the condition to a string
        condition = str(condition)
        switch = {
            'start': 'Start', # Covers StartTimeFrom and StartTimeTo - Items that started in this date range
            'mod': 'Mod', # Covers ModTimeFrom and ModTimeTo - Items modified in this date range
            'end': 'End' # Covers EndTimeFrom and EndTimeTo - Items that end within this date range
        }
        # Return the approiate text or End if no ID matches
        return switch.get(condition, 'End')
                
    # Override the dates in sellerList with values from dateRange, if provided  
    if ('from' in range) & ('type' in range):
        # Remove the list of keys from sellerList - We can only have one search range
        for o in ['EndTimeFrom','ModTimeFrom','StartTimeFrom']:
            try:
                del list[o]
            except KeyError:
                continue

        list[rangeType(range['type'])+'TimeFrom'] = range['from']

    if ('to' in range) & ('type' in range):
        # Remove the list of keys from sellerList - We can only have one search range
        for o in ['EndTimeTo','ModTimeTo','StartTimeTo']:
            try:
                del list[o]
            except KeyError:
                continue

        list[rangeType(range['type'])+'TimeTo'] = range['to']

    return list