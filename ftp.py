import ftplib

config = {
	'host': '',
	'port': 21,
	'user': '',
	'pass': '',
	'tls': True,
	'file_path': [
		''
	]
}

if config['tls']:
	ftp = ftplib.FTP_TLS()
else:
	ftp = ftplib.FTP()

ftp.connect(config['host'], config['port'])
print ftp.getwelcome()

#try:
print 'Logging into {} with username {}'.format(config['host'], config['user'])
ftp.login(config['user'], config['pass'])
#ftp.prot_p()

for i in config['file_path']:
	try:
		file = open(i, 'rb')

		# Get the filename
		fname = i.split('/')
		fname = fname[len(fname)-1]

		print 'Uploading {} to {}'.format(fname, config['host'])

		ftp.storlines('STOR {}'.format(fname), file)
		file.close()

		print 'Done uploading {}'.format(fname)
	except IOError:
		print '{} does not exist or is unreadable - Skipping'.format(fname)
