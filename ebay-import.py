from ebaysdk.trading import Connection as Trading
from ebaysdk.exception import ConnectionError, ConnectionResponseError
import psycopg2 as db

import datetime
import time
import csv
import re

import ebayWrapper
import dateUtils
import config
import sql


print 'Imported required modules'

try:
    currentDate = datetime.datetime.strptime(config['ebay']['startDate'], '%Y-%m-%d')
    endDate = datetime.datetime.strptime(config['ebay']['endDate'], '%Y-%m-%d')
except KeyError:
    if config['ebay']['startDate'] == 'yyyy-mm-dd': 
        config['ebay']['startDate'] = datetime.datetime.today()
    if config['ebay']['endDate'] == 'yyyy-mm-dd': 
        config['ebay']['endDate'] = datetime.datetime.today()



itemTotal = []
badItems = []



try:
    
    

    with open(csv_path, 'a') as csvfile:

        fieldnames = ['Part Number', 'Manufacturer', 'Condition', 'Price', 'Quantity', 'Description']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n', delimiter=csv_delimiter, quotechar=csv_quote, quoting=csv.QUOTE_MINIMAL)
        writer.writerow({'Part Number':'Part Number', 'Manufacturer':'Manufacturer', 'Condition':'Condition', 'Price':'Price', 'Quantity':'Quantity', 'Description':'Description'})

        while endDate <= currentDate:
            # Stop the loop well before we hit our rate limit
            if len(itemTotal) >= rate_limit:
                print 'ItemTotal has reached {}, stopping to avoid rateLimits'.format(rate_limit)
                break

            dateRange = setDateRange(days=-20, start=currentDate, rangeType='start')

            itemData = glue(api=api, sellerList={}, dateRange=dateRange)
            itemlist = []

            for i in itemData:

                def keyCheck(key):
                    try:
                        data = itemData[i][key]
                        return data
                    except KeyError:
                        data = 'N/a'
                        return data

                try:
                    price = itemData[i]['price']['val']
                except KeyError:
                    price = 'N/a'

                try:
                    quantity = str(int(itemData[i]['quantity']['total'])-int(itemData[i]['quantity']['sold']))
                except KeyError:
                    quantity = 0

                try:
                    condition = itemData[i]['condition']['msg']
                except KeyError:
                    condition = 'N/a'

                data = {
                    'Part Number': keyCheck('mpn'),
                    'Manufacturer': keyCheck('mfg'),
                    'Condition': condition,
                    'Price': price,
                    'Quantity': quantity,
                    'Description': keyCheck('title').encode('utf-8')
                }

                try:
                    writer.writerow(data)

                    with open(dblog_path, 'a') as dblog:
                        fieldnames = ['create_date', 'name', 'description', 'manufacturer', 'part_number', 'condition', 'ebay_id', 'location_id', 'quantity', 'list_price', 'product_tmpl_id', 'product_id', 'id', 'location']
                        logWrite = csv.DictWriter(dblog, fieldnames=fieldnames, lineterminator='\n', delimiter=csv_delimiter, quotechar=csv_quote, quoting=csv.QUOTE_MINIMAL)
                        # Prepare our data for SQL
                        data = {
                            'create_date': datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S.%f"),
                            'name': keyCheck('title').encode('utf-8'),
                            'description': 'Imported from ebay',
                            'manufacturer': keyCheck('mfg'),
                            'part_number': keyCheck('mpn'),
                            'condition': condition,
                            'ebay_id': i,
                            'location_id': 600, # This is the ID of the default location in the database
                            'quantity': quantity,
                            'list_price': price,
                            'location': keyCheck('location') 
                        }

                        logWrite.writerow(importToDB(data))


                except UnicodeEncodeError:
                    badItems.append(i)
                    continue

                itemlist.append(i)
                itemTotal.append(i)

            print 'Current Total Processed Items: {} - Processed {} items in Current DateRange From: {} To: {} '.format(len(itemTotal), len(itemlist), re.sub('T(.*?)Z', '', dateRange['from']), re.sub('T(.*?)Z', '', dateRange['to']))
            # Take the starting date from the dateRange (remove the time (THH:MM:SS.sssZ) with regex) and feed it back to the start of the loop
            currentDate = datetime.datetime.strptime(re.sub('T(.*?)Z', '', dateRange['from']), '%Y-%m-%d')
           
            if 'wait_time' in config:           
                # Sleep for config.wait_time
                time.sleep(config['wait_time'])
            else:
                # Defaults to making one series of calls every 60 seconds
                time.sleep(60)

    csvfile.close()
    con.close()


    # Log the imported itemIDs into a csv file 
    with open(item_csv_path, 'a') as csvitems:
        fieldnames = ['ItemID']
        writer = csv.DictWriter(csvitems, fieldnames=fieldnames, lineterminator='\n', delimiter=csv_delimiter, quotechar=csv_quote, quoting=csv.QUOTE_MINIMAL)
        writer.writerow({'ItemID':'ItemID'})

        print 'Logging the ItemIDs that were imported'

        for i in itemTotal:
            writer.writerow({'ItemID':i})
    csvitems.close()

    # Log the itemIDs that are giving unicode problems into a csv file 
    with open(bad_item_csv_path, 'a') as csvitemsbad:
        fieldnames = ['ItemID']
        writer = csv.DictWriter(csvitemsbad, fieldnames=fieldnames, lineterminator='\n', delimiter=csv_delimiter, quotechar=csv_quote, quoting=csv.QUOTE_MINIMAL)
        writer.writerow({'ItemID':'ItemID'})

        print 'Logging the ItemIDs that failed to import due to unicode characters'

        for i in badItems:
            writer.writerow({'ItemID':i})
    csvitemsbad.close()

    print 'All done!'

except ConnectionError as e:
    print(e)