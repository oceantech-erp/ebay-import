# ebay import functions

# Here are the error codes that we currently have
# None - No events have happened yet
# 0 - Everything is fine
# 1 - The API variable is either unset, or is not a valid connection to the ebay API
# 2 - The required list is either unset, or is not the required type - Usually dict, but can be a list of dicts
# 3 - EndTimeFrom or EndTimeTo were not set in the list - getSeller only
# 4 - Relates to 3, but specifically refers to a dateRange not being available for glue
# 5 - No data (useful) returned by API - A field of data we need is None

# minor logic checking - We don't want to get trackbacks for silly reasons
# getSeller gets a list of our itemIDs
def getSeller(api=None, list=None):
    # api is the connection to the Trading API
    # list is a dict containing required information for ebay to search for (http://developer.ebay.com/DevZone/XML/docs/Reference/ebay/GetSellerEvents.html)
    #   - The datapoints that MUST be defined are EndTimeFrom and EndTimeTo - These select the dateRange that items end on ebay
    
    # Set our error conditions
    res = { 'error': { 'code': None, 'msg': None, 'fnc': 'getSeller' }, 'apiResponse': {} }
    if api == None:
        res['error']['code'] = '1'
        res['error']['msg'] = 'api is not set'
        return res
    if (list == None) | (isinstance(list, dict) != True):
        res['error']['code'] = '2'
        res['error']['msg'] = 'list doesn\'t exist or is of wrong type, must be dict'
        return res
    if (('EndTimeFrom' not in list) | ('EndTimeTo' not in list)) & (('StartTimeFrom' not in list) | ('StartTimeTo' not in list)):
        res['error']['code'] = '3'
        res['error']['msg'] = 'either "StartTime" or "EndTime" is not set in list'
        return res

    # If arguments have been passed through and the arguments are a dict
    if ('args' in list) & (type(list['args']) == type({})):
        # Create a new dict in the response to send to functions down the line
        res['apiResponse']['args'] = {}
        for i in list['args']:
            res['args'][i] = list['args'][i]

    print "Getting Seller Events for dateRange {} to {}".format(list['StartTimeFrom'], list['StartTimeTo'])

    # Call GetSellerEvents and store the resulting dict into res['apiResponse']
    res['apiResponse'] = api.execute('GetSellerEvents', list).dict()
    
    # Verify that the search returned information
    if res['apiResponse']['ItemArray'] != None:
        # Check if the itemArray is setup the way we want it (list containing one or more dicts)
        try:
            for k in res['apiResponse']['ItemArray']['Item']:
                itemid = k['ItemID']
        except TypeError:
            # The itemArray is not, force it to be then
            res['apiResponse'] = [ res['apiResponse']['ItemArray']['Item'] ]
        else:
            # No need to encase the list in another list
            res['apiResponse'] = res['apiResponse']['ItemArray']['Item']
        
        # Yay no errors
        if res['error']['code'] == None:
            res['error']['code'] = '0'
    else:
        # drop the response as it contains no useful information anymore
        res['apiResponse'] = {} 
        res['error']['code'] = '5'
        res['error']['msg'] = 'no items found - maybe the dateRange is too narrow?'
        
    return res

# getItems uses the list of ItemIDs provided by getSeller to get specific information about each ItemID
def getItems(api=None, itemList=None, itemArgs=None):
    # api is the connection to the Trading API
    # itemList is a dict containing ItemIDs and other info as a result of getSeller
    # itemArgs is an optional dict that contains extra details to refine the search returned by ebay (http://developer.ebay.com/DevZone/XML/docs/Reference/ebay/GetItem.html)
    #   - The two required datapoints in itemArgs are IncludeItemSpecifics and ItemID, both are defined below in the loop
    
    # Set our error conditions
    res = { 'error': { 'code': None, 'msg': None, 'fnc': 'getItems' }, 'apiResponse': {} }
    if api == None:
        res['error']['code'] = '1'
        res['error']['msg'] = 'api is not set'
        return res
    if (itemList == None) | (type(itemList) != type([])):
        res['error']['code'] = '2'
        res['error']['msg'] = 'itemList doesn\'t exist or is of wrong type, must be list containing one or more dicts'
        return res
    if isinstance(itemArgs, dict) != True:
        res['error']['code'] = '2'
        res['error']['msg'] = 'itemArgs doesn\'t exist or is of wrong type, must be dict'
        return res

        
    # For each ItemID
    for k in itemList:
        # Don't process args 
        if k == 'args':
            continue

        # Define default extra arguments
        itemArgs['IncludeItemSpecifics'] = 'True'

        # If arguments have been passed and they are a dictionary
        if ('args' in itemList) & (type(itemList['args']) == type({})):
            # If an itemID that is a string with more than a zero length is defined as an argument
            # TODO - Change itemID to a list to be able to process a range of IDs instead of having to run the script multiple times
            if ('itemID' in itemList['args']) & ((type(itemList['args']['itemID']) == type(str)) & (len(itemList['args']['itemID']) > 0)):
                # If the specific itemID was already done
                if ('itemDone' in itemList['args']) & (itemList['args']['itemDone'] == True):
                    # We're done here, process the others
                    continue
                else:
                    # Search for the ID from the argument and set itemDone to True for future iterations
                    itemArgs['ItemID'] = itemList['args']['itemID']
                    itemList['args']['itemDone'] = True
        else:
            # Default to using the itemID retrieved from getSeller
            itemArgs['ItemID'] = k['ItemID']

        # If the itemID is marked as Active on ebay or is supplied as an argument
        if (k['SellingStatus']['ListingStatus'] == 'Active') | ('itemID' in itemList['args']):
            # Call GetItem and store the resulting dict into res['apiResponse'][ItemID]
            res['apiResponse'][itemArgs['ItemID']] = api.execute('GetItem', itemArgs).dict()
        else:
            if isInDB(k['ItemID']):
                # Mark the item as archived and remove saleable ability - This is the same as removing the item, but its data is still available for previously created invoices
                sql_archive(k['ItemID'])
            continue
        
        # We want the error code to only be changed on the first successful iteration
        if res['error']['code'] == None:
            res['error']['code'] = '0'
    
    # After the loop is complete, return the whole res
    return res

# storeItems uses the dict of Items provided by getItems and stores the information we want 
def storeItems(itemList=None):
    # itemList is the apiRequest value presented as a result of getItems 
    #   - This contains every item that matches getItems criteria with the ItemID as the key of further dicts

    # Set our error conditions
    res = { 'error': { 'code': None, 'msg': None, 'fnc': 'storeItems' }, 'apiResponse': {} }
    if (itemList == None) | (isinstance(itemList, dict) != True):
        res['error']['code'] = '2'
        res['error']['msg'] = 'itemList doesn\'t exist or is of wrong type, must be dict'
        return res
    
    # Now that we've stored all the data in a really big dictionary, lets pull only the information we want out of it
    for k in itemList:
        res['apiResponse'][k] = { 'price': {}, 'condition': {}, 'quantity': {} }
        condition = '0'
        
        # Switch statement - Sets condition.msg based on conditionID - based on table from http://developer.ebay.com/devzone/finding/callref/Enums/conditionIdList.html
        def conCheck(condition):
            # Cast the condition to a string
            condition = str(condition)
            switch = {
                '1000': 'New',
                '1500': 'New Other',
                '1750': 'New with defects',
                '2000': 'Manufacturer Refurbished',
                '2500': 'Seller Refurbished',
                '3000': 'Used',
                '4000': 'Used/Very Good Condition',
                '5000': 'Used/Good Condition',
                '6000': 'Used/Acceptable Condition',
                '7000': 'For Parts/Not Working'
            }
            # Return the approiate text or N/A if no ID matches
            return switch.get(condition, 'N/A')
        
        res['apiResponse'][k]['title'] = itemList[k]['Item']['Title']

        if 'SKU' in itemList[k]['Item']:
            res['apiResponse'][k]['location'] = itemList[k]['Item']['SKU']
        else:
            res['apiResponse'][k]['location'] = None
        
        if 'ConditionID' in itemList[k]['Item']:
            # Override the condition defined at the start of the loop
            condition = itemList[k]['Item']['ConditionID']
        
        if 'SellingStatus' in itemList[k]['Item']:
            # Store current price + currency the price is in
            res['apiResponse'][k]['price']['cur'] = itemList[k]['Item']['SellingStatus']['CurrentPrice']['_currencyID']
            res['apiResponse'][k]['price']['val'] = itemList[k]['Item']['SellingStatus']['CurrentPrice']['value']
            
            # Store the quantity - Subtract sold from total to get current value
            res['apiResponse'][k]['quantity']['sold'] = itemList[k]['Item']['SellingStatus']['QuantitySold']
            res['apiResponse'][k]['quantity']['total'] = itemList[k]['Item']['Quantity']
        else:
            res['apiRequest'][k]['price']['cur'] = None
            res['apiRequest'][k]['price']['val'] = None
            res['apiRequest'][k]['quantity']['sold'] = None
            res['apiRequest'][k]['quantity']['total'] = None
        
        # Get the item specifics - Special fields pertaining to the item, such as manufacturer and part number
        if 'ItemSpecifics' in itemList[k]['Item']:
            # Try to for loop our data, if that fails, assume it's a dict with only one ItemSpecific
            try:
                for i in itemList[k]['Item']['ItemSpecifics']['NameValueList']:
                    name = i['Name']
                    if(name == 'Brand'):
                        res['apiResponse'][k]['mfg'] = i['Value']
                    if(name == 'MPN'):
                        res['apiResponse'][k]['mpn'] = i['Value']
            except TypeError:
                if itemList[k]['Item']['ItemSpecifics']['NameValueList']['Name'] == 'Brand':
                    res['apiResponse'][k]['mfg'] = itemList[k]['Item']['ItemSpecifics']['NameValueList']['Value']
                    res['apiResponse'][k]['mpn'] = None
                elif itemList[k]['Item']['ItemSpecifics']['NameValueList']['Name'] == 'MPN':
                    res['apiResponse'][k]['mpn'] = itemList[k]['Item']['ItemSpecifics']['NameValueList']['Value']
                    res['apiResponse'][k]['mfg'] = None
                else:
                    res['apiResponse'][k]['mpn'] = None
                    res['apiResponse'][k]['mfg'] = None
                    
        # Store the condition
        res['apiResponse'][k]['condition']['code'] = condition
        res['apiResponse'][k]['condition']['msg'] = conCheck(condition)
                
        # We want the error code to only be changed on the first successful iteration
        if res['error']['code'] == None:
            res['error']['code'] = '0'

    # After the loop is complete, return the whole res
    return res

# Glue logic to run all the functions above properly
def glue(api=None, sellerList=None, dateRange=None):
    # api is the connection to the api, this is passed to the functions as called
    # sellerList is the options that you can present ebay for searching (http://developer.ebay.com/DevZone/XML/docs/Reference/ebay/GetSellerEvents.html)
    # dateRange overrides EndTimeFrom and EndTimeTo from sellerList for ease of passing these required data with nothing else

    # Set our error conditions
    res = { 'error': { 'code': '0', 'msg': None, 'fnc': 'glue' }, 'apiResponse': {} }
    if api == None:
        res['error']['code'] = '1'
        res['error']['msg'] = 'api is not set'
        return res
    if (sellerList == None) | (isinstance(sellerList, dict) != True):
        res['error']['code'] = '2'
        res['error']['msg'] = 'itemList doesn\'t exist or is of wrong type, must be dict'
        return res
    if (dateRange == None) | (isinstance(dateRange, dict) != True):
        dateRange = setDateRange()

    # Switch statement to select the proper dateRange
    sellerList = switchDateRange(sellerList, dateRange)
    
    seller = getSeller(api, sellerList)
    if seller['error']['code'] == '0':
        sellerList = seller['apiResponse']
        seller = None # Unset any data we no-longer need - Saves on memory
            
        items = getItems(api, sellerList, {})
        if items['error']['code'] == '0':
            sellerList = items['apiResponse']
            
            
            items = storeItems(sellerList)
            storedItems = items['apiResponse']
            items = None
            
            return storedItems
        else:
            res['error'] = items['error']
            return res
    else:
        res['error'] = seller['error']
        return res


# Gets the totals of what we have on ebay
def getItemTotals(api=None, list=None):
    # api is the connection to the Trading API
    # list is a dict containing information to search for and can have any combination of the following:
    #   Active - Bid - Sold - Scheduled - Unsold - DeletedSold

    res = { 'error': { 'code': None, 'msg': None, 'fnc': 'getItemTotals' }, 'apiResponse': {} }
    if api == None:
        res['error']['code'] = '1'
        res['error']['msg'] = 'api is not set'
        return res
    if (list == None) | (type(list) != type([])):
        res['error']['code'] = '2'
        res['error']['msg'] = 'list doesn\'t exist or is of wrong type'
        return res

    # Switch Statement - Compares the items in list for accepted keywords
    def conCheck(condition):
        # Cast the condition to a lowercase string
        condition = str(condition).lower()
        switch = {
            'active': 'ActiveList',              # All active items
            'bid': 'BidList',                    # Items that have bids
            'sold': 'SoldList',                  # Items that were sold
            'unsold': 'UnsoldList',              # Items that were ended without being sold
            'scheduled': 'ScheduledList',        # Listed items that are scheduled to become active later
            'deletedsold': 'DeletedFromSoldList' # Sold items that were deleted from My eBay Dashboard
        }
        # Return the approiate text or N/A if no ID matches
        return switch.get(condition, None)

    # Convert list into a dict so we can feed that into the ebay api
    toGet = {'DetailLevel':'ReturnHeaders'}
    for i in list:
        toGet[conCheck(i)] = True
    
    res['apiResponse'] = api.execute('GetMyeBaySelling', toGet).dict()

    if res['apiResponse']['Ack'] == 'Success':
        for i in list:
            getItems = conCheck(i)
            getItems = res['apiResponse'][getItems]['PaginationResult']['TotalNumberOfEntries']

            print "Found {} Total {} items".format(getItems, i)

        res['error']['code'] = '0'
        return res


try:
    ebay = config['ebay']['application'] # attempts to pull the ebay application config from the config file
    try:
        ebay_api = Trading(domain=ebay['domain'], appid=ebay['app_id'], devid=ebay['dev_id'], certid=ebay['crt_id'], token=ebay['usr_token'], config_file=None, debug=False) # Opens a connection to the ebay API
        print "Connected to the ebay API located at: {} with application: {}".format(ebay['domain'], ebay['app_id'])
        ebay = None # unset the configuration
    except ConnectionError as e:
        print(e)
except KeyError: # it failed
    raise KeyError 'ebay settings missing from config file!'

